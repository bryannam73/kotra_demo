Element.prototype.hasClass = function (className) {
	return this.className && new RegExp('(^|s)' + className + '(s|$)').test(this.className);
};

// Key Visual Banner PC 01 - 빅 와이드 배너(Auto Slide) + 좌측 스몰 배너(Slide)
function loadSwiperKeyvisual01(el) {
	var swiperList1 = getAll(el + ' .main-key-visual1 .swiper-slide'),
		swiperControlWrap = getAll(el + ' .main-key-visual1 .swiper-control-wrapper');
	if (swiperList1.length > 1) {
		var swiperKeyVisual = new Swiper(el + ' .main-key-visual1', {
			loop: true,
			simulateTouch: false,
			autoplay: {
				delay: 3000,
			},
			speed: 1000,
			// speed: 600,
			parallax: true,
			// grabCursor: true,
			watchSlidesProgress: true,
			mousewheelControl: false,
			keyboardControl: true,

			pagination: {
				el: el + ' .main-key-visual1 .swiper-pagination',
				type: 'progressbar',
			},
			navigation: {
				nextEl: el + ' .swiper-control-wrapper .swiper-button-next',
				prevEl: el + ' .swiper-control-wrapper .swiper-button-prev',
			},
			on: {
				slideChangeTransitionStart: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .main-key-visual1');
					var duplicateNum = $this.querySelectorAll('.swiper-slide-duplicate').length;
					var totalNum = Number(swiper.slides.length) - Number(duplicateNum);
					var currentNum = Number($this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex) + 1;
					$this.querySelector('.swiper-pagination-total').innerText = totalNum;
					$this.querySelector('.swiper-pagination-current').innerText = currentNum;
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .main-key-visual1');
					var sButton = $this.querySelector('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.hasClass('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
	} else {
		swiperList1.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}

	// 메인 키비주얼2 슬라이드
	var swiperList2 = getAll(el + '  .main-key-visual2 .swiper-slide');
	if (swiperList1.length > 1) {
		var swiperKeyVisual2 = new Swiper(el + ' .main-key-visual2 .swiper-container', {
			// speed: 0,
			loop: true,
			simulateTouch: false,
			// preloadImages: false,
			// lazy: true,
			spaceBetween: 0,
			slidesPerView: 1,
			pagination: {
				el: el + ' .main-key-visual2 .swiper-pagination-count',
				type: 'custom',
				renderCustom: function (swiper, current, total) {
					function numberAppend(d) {
						return d < 10 ? '0' + d.toString() : d.toString();
					}
					return '<span class="swiper-pagination-current">' + numberAppend(current) + '</span> /  <span class="swiper-pagination-total">' + numberAppend(total) + '</span>';
				},
			},
			navigation: {
				nextEl: el + ' .main-key-visual2 .swiper-button-next',
				prevEl: el + ' .main-key-visual2 .swiper-button-prev',
			},
		});
	} else {
		swiperList2.style.display = 'block';
		$(el + ' .main-key-visual2 .swiper-pagination-count').style.display = 'none';
		$(el + ' .main-key-visual2 .swiper-button-prev').style.display = 'none';
		$(el + ' .main-key-visual2 .swiper-button-next').style.display = 'none';
	}

	document.querySelector(el + ' .swiper-button-pause').addEventListener('click', function (e) {
		if (e.target.hasClass('.__state-swiper-play')) {
			// $(this).removeClass('__state-swiper-play').addClass('__state-swiper-pause').find('i').removeClass('icon-control-play').addClass('icon-control-pause');
			swiperKeyVisual.autoplay.start();
		} else {
			// $(this).removeClass('__state-swiper-pause').addClass('__state-swiper-play').find('i').removeClass('icon-control-pause').addClass('icon-control-play');
			swiperKeyVisual.autoplay.stop();
		}
	});
}

// Key Visual Banner PC 02 - 3단 다이나믹 배너(Auto Slide)
function loadSwiperKeyvisual02(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .swiper-control-box-wrap');

	if (swiperList.length > 2) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			autoplay: {
				delay: 5000,
			},
			speed: 400,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			slidesPerView: 3,
			centeredSlides: true,
			freeMode: true,
			spaceBetween: 50,
			slidesPerGroup: 1,
			on: {
				init: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var duplicateNum = $this.querySelector('.swiper-slide-duplicate').length;
					var totalNum = swiper.slides.length - duplicateNum;
					var bulletNum;
					for (var i = 1; i <= totalNum; i++) {
						if (i === 1) {
							// add active class if it is the first bullet
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							// $(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number swiper-pagination-number-active slide' + i + '">' + bulletNum + '</span>');
						} else {
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							// $(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number slide' + i + '">' + bulletNum + '</span>');
						}
					}
				},
				slideChangeTransitionStart: function () {
					// Get current slide from fraction pagination number
					var $this = document.querySelector(el);
					var currentNum = $this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex + 1;
					var bullets = $this.querySelector(el + ' .swiper-pagination-number');
					// console.log(bullets);

					// Remove active class from all bullets
					// bullets.classList.remove('swiper-pagination-number-active');
					// Check each bullet element if it has slideNumber class
					// $.each(bullets, function (index, value) {
					// 	var slideClass = 'slide' + (index + 1);
					// 	if (currentNum >= index + 1) {
					// 		$(this).addClass('swiper-pagination-number-active');
					// 	}
					// });
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .swiper-container');
					var sButton = $this.querySelector('.swiper-button-pause');
					if (sButton.length) {
						document.getElementById('selector').hasClass('className');

						if (sButton.is('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
		// $(document).on('click', el + ' .swiper-button-pause', function () {
		// 	if ($(this).is('.__state-swiper-play')) {
		// 		$(this).removeClass('__state-swiper-play').addClass('__state-swiper-pause').find('i').removeClass('icon-control-play').addClass('icon-control-pause');
		// 		swiperObj.autoplay.start();
		// 	} else {
		// 		$(this).removeClass('__state-swiper-pause').addClass('__state-swiper-play').find('i').removeClass('icon-control-pause').addClass('icon-control-play');
		// 		swiperObj.autoplay.stop();
		// 	}
		// });
	} else {
		swiperList.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}
}

// Key Visual Banner PC 03 - 빅와이드 배너(Slide) : 520px
function loadSwiperWideBanner03(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .swiper-control-box-wrap');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			autoplay: {
				delay: 5000,
			},
			speed: 800,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			on: {
				init: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var duplicateNum = $this.querySelector('.swiper-slide-duplicate').length;
					var totalNum = swiper.slides.length - duplicateNum;
					var bulletNum;
					for (var i = 1; i <= totalNum; i++) {
						if (i === 1) {
							// add active class if it is the first bullet
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number swiper-pagination-number-active slide' + i + '">' + bulletNum + '</span>');
						} else {
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number slide' + i + '">' + bulletNum + '</span>');
						}
					}
				},
				slideChangeTransitionStart: function () {
					// Get current slide from fraction pagination number
					var $this = document.querySelector(el);
					var currentNum = $this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex + 1;
					var bullets = $this.querySelector(el + ' .swiper-pagination-number');
					// Remove active class from all bullets
					// bullets.removeClass('swiper-pagination-number-active');
					// Check each bullet element if it has slideNumber class
					// $.each(bullets, function (index, value) {
					// 	var slideClass = 'slide' + (index + 1);
					// 	if (currentNum >= index + 1) {
					// 		$(this).addClass('swiper-pagination-number-active');
					// 	}
					// });
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .swiper-container');
					var sButton = $this.querySelector('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
		// $(document).on('click', el + ' .swiper-button-pause', function () {
		// 	if ($(this).is('.__state-swiper-play')) {
		// 		$(this).removeClass('__state-swiper-play').addClass('__state-swiper-pause').find('i').removeClass('icon-control-play').addClass('icon-control-pause');
		// 		swiperObj.autoplay.start();
		// 	} else {
		// 		$(this).removeClass('__state-swiper-pause').addClass('__state-swiper-play').find('i').removeClass('icon-control-pause').addClass('icon-control-play');
		// 		swiperObj.autoplay.stop();
		// 	}
		// });
	} else {
		swiperList.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}
}

// Key Visual Banner PC 04 - 빅와이드 배너(Slide) + Bullet
function loadSwiperWideBanner04(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .swiper-controls-wrap');

	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			speed: 800,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				clickable: true,
			},
		});
	} else {
		swiperList.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}
}

// WideBanner
function loadSwiperWideBanner05(el) {
	var swiperList = getAll(el + ' .swiper-slide');
	if (swiperList.length > 1) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			spaceBetween: 0,
			slidesPerView: 'auto',
			on: {
				init: function () {
					$('.section-branch .tab-block-item').on('click', function () {
						var idx = $(this).index();
						swiperObj.slideToLoop(idx);
					});
				},
				slideChangeTransitionStart: function () {
					var loopIndex = $(el + ' .swiper-slide-active').attr('data-swiper-slide-index');
					$('.section-branch .tab-block-item').removeClass('is-active').eq(loopIndex).addClass('is-active');
				},
			},
		});
	}
}

// 일반 autoplay 용 4단 슬라이드
function loadSwiperAutoCol4(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControl1 = getAll(el + ' .swiper-button-prev'),
		swiperControl2 = getAll(el + ' .swiper-button-next');

	if (swiperList.length > 4) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			autoplay: {
				delay: 3000,
			},
			paginationClickable: true,
			spaceBetween: 28,
			slidesPerView: 4,
			slidesPerGroup: 4,
		});
		$(document)
			.on('mouseenter', el, function () {
				swiperObj.autoplay.stop();
			})
			.on('mouseleave', el, function () {
				swiperObj.autoplay.start();
			});
	} else {
		swiperList.style.display = 'block';
		swiperControl1.style.display = 'none';
		swiperControl2.style.display = 'none';
	}
}

// 배너 전체 보기 팝업용 멀티 컬럼 슬라이드
function loadSwiperMultiCol01(el) {
	let swiperContainer = document.querySelector(el),
		swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = document.querySelector(el + ' .paging-container');

	if (swiperList.length > 4) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			loop: false,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				type: 'progressbar',
			},
			// slidesPerView: 4,
			// slidesPerGroup: 4,
			// spaceBetween: 0,
			slidesPerView: 2,
			slidesPerColumn: 2,
			spaceBetween: 30,
			on: {
				init: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var totalNum = (swiper.slides.length % 4) + 2;
					var currentNum = swiper.activeIndex + 1;
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this.find('.swiper-pagination-total').text(totalNum.slice(-2));
					$this.find('.swiper-pagination-current').text(currentNum.slice(-2));
				},
				slideChangeTransitionStart: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var totalNum = (swiper.slides.length % 4) + 2;
					var currentNum = swiper.activeIndex + 1;
					totalNum = '0' + totalNum;
					currentNum = '0' + currentNum;
					$this.find('.swiper-pagination-total').text(totalNum.slice(-2));
					$this.find('.swiper-pagination-current').text(currentNum.slice(-2));
				},
			},
		});
	} else {
		swiperContainer.style.display = 'block';
		// swiperControlWrap.style.display = 'none';
	}
}

// Category 슬라이드
function loadSwiperCate(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .section-category-control-wrap');

		if (swiperList.length > 2) {
			var swiperObj = new Swiper(el + ' .swiper-container', {
				simulateTouch: false,
				autoplay: false,
				speed: 400,
				loop: true,
				navigation: {
					nextEl: el + ' .swiper-button-next',
					prevEl: el + ' .swiper-button-prev',
				},
				pagination: {
					el: el + ' .swiper-pagination',
					clickable: true,
				},
				centeredSlides: false,
				freeMode: true,
				slidesPerView: 9,
				spaceBetween: 15,
				slidesPerGroup: 9,
				breakpoints: {
					// 360
					280: {
						slidesPerView: 2,
						slidesPerGroup: 2,
					},
					520: {
						slidesPerView: 4,
						slidesPerGroup: 4,
					},
					820: {
						slidesPerView: 6,
						slidesPerGroup: 6,
					},
					1041: {
						slidesPerView: 9,
					}
				},
				on: {
					init: function () {
						var swiper = this;
						$this = document.querySelector(el);
						var duplicateNum = $this.querySelector('.swiper-slide-duplicate').length;
						var totalNum = swiper.slides.length - duplicateNum;
						var bulletNum;
						for (var i = 1; i <= totalNum; i++) {
							if (i === 1) {
								// add active class if it is the first bullet
								bulletNum = '0' + i;
								bulletNum = bulletNum.slice(-2);
								$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number swiper-pagination-number-active slide' + i + '">' + bulletNum + '</span>');
							} else {
								bulletNum = '0' + i;
								bulletNum = bulletNum.slice(-2);
								$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number slide' + i + '">' + bulletNum + '</span>');
							}
						}
					},
					slideChangeTransitionStart: function () {
						// Get current slide from fraction pagination number
						var $this = document.querySelector(el);
						var currentNum = $this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex + 1;
						var bullets = $this.querySelector(el + ' .swiper-pagination-number');
					},
					slideChangeTransitionEnd: function () {
						var swiper = this;
						var $this = document.querySelector(el + ' .swiper-container');
						var sButton = $this.querySelector('.swiper-button-pause');
						if (sButton.length) {
							if (sButton.is('.__state-swiper-pause')) {
								swiper.autoplay.start();
							}
						}
					},
				},
			});
		} else {
			swiperList.style.display = 'block';
			swiperControlWrap.style.display = 'none';
		}

}

// product 슬라이드 7
function loadSwiperProduct(el, col, gap) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .section-category-control-wrap');

	if (swiperList.length > 2) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			autoplay: false,
			// {
			// 	delay: 5000,
			// },
			speed: 400,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				clickable: true,
			},
			centeredSlides: false,
			freeMode: true,
			slidesPerView: col,
			spaceBetween: gap,
			slidesPerGroup: col,
			breakpoints: {
				// 360
				280: {
					slidesPerView: 2,
					slidesPerGroup: 2,
				},
				520: {
					slidesPerView: 3,
					slidesPerGroup: 3,
				},
				820: {
					slidesPerView: 4,
					slidesPerGroup: 4,
				},
				1041: {
					slidesPerView: col,
				}
			},
			on: {
				init: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var duplicateNum = $this.querySelector('.swiper-slide-duplicate').length;
					var totalNum = swiper.slides.length - duplicateNum;
					var bulletNum;
					for (var i = 1; i <= totalNum; i++) {
						if (i === 1) {
							// add active class if it is the first bullet
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number swiper-pagination-number-active slide' + i + '">' + bulletNum + '</span>');
						} else {
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number slide' + i + '">' + bulletNum + '</span>');
						}
					}
				},
				slideChangeTransitionStart: function () {
					// Get current slide from fraction pagination number
					var $this = document.querySelector(el);
					var currentNum = $this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex + 1;
					var bullets = $this.querySelector(el + ' .swiper-pagination-number');
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .swiper-container');
					var sButton = $this.querySelector('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
	} else {
		swiperList.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}
}


// product 슬라이드 5
function loadSwiperComProduct(el) {
	var swiperList = getAll(el + ' .swiper-slide'),
		swiperControlWrap = getAll(el + ' .section-category-control-wrap');

	if (swiperList.length > 2) {
		var swiperObj = new Swiper(el + ' .swiper-container', {
			simulateTouch: false,
			autoplay: false,
			// {
			// 	delay: 5000,
			// },
			speed: 400,
			loop: true,
			navigation: {
				nextEl: el + ' .swiper-button-next',
				prevEl: el + ' .swiper-button-prev',
			},
			pagination: {
				el: el + ' .swiper-pagination',
				clickable: true,
			},
			centeredSlides: false,
			freeMode: true,
			slidesPerView: 5,
			spaceBetween: 25,
			slidesPerGroup: 5,
			breakpoints: {
				// 360
				280: {
					slidesPerView: 2,
					slidesPerGroup: 2,
				},
				520: {
					slidesPerView: 3,
					slidesPerGroup: 3,
				},
				820: {
					slidesPerView: 4,
					slidesPerGroup: 4,
				},
				1041: {
					slidesPerView: 5,
				}
			},
			on: {
				init: function () {
					var swiper = this;
					$this = document.querySelector(el);
					var duplicateNum = $this.querySelector('.swiper-slide-duplicate').length;
					var totalNum = swiper.slides.length - duplicateNum;
					var bulletNum;
					for (var i = 1; i <= totalNum; i++) {
						if (i === 1) {
							// add active class if it is the first bullet
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number swiper-pagination-number-active slide' + i + '">' + bulletNum + '</span>');
						} else {
							bulletNum = '0' + i;
							bulletNum = bulletNum.slice(-2);
							$(el + ' .swiper-pagination-bullet-wrap').append('<span class="swiper-pagination-number slide' + i + '">' + bulletNum + '</span>');
						}
					}
				},
				slideChangeTransitionStart: function () {
					// Get current slide from fraction pagination number
					var $this = document.querySelector(el);
					var currentNum = $this.querySelector('.swiper-slide-active').dataset.swiperSlideIndex + 1;
					var bullets = $this.querySelector(el + ' .swiper-pagination-number');
				},
				slideChangeTransitionEnd: function () {
					var swiper = this;
					var $this = document.querySelector(el + ' .swiper-container');
					var sButton = $this.querySelector('.swiper-button-pause');
					if (sButton.length) {
						if (sButton.is('.__state-swiper-pause')) {
							swiper.autoplay.start();
						}
					}
				},
			},
		});
	} else {
		swiperList.style.display = 'block';
		swiperControlWrap.style.display = 'none';
	}
}