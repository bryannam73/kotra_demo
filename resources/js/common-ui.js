'use strict';
// 인풋, 텍스트 글자수 카운트
let setTextCount = function (id, max) {
	let el = document.getElementById(id);
	el.addEventListener('keyup', function (e) {
		e.stopPropagation();
		let count = Number(el.value.length);
		if (count > max) {
			el.value = el.value.substr(0, max);
			alert('글자 수가' + String(max) + '자를 초과했습니다.');
		} else {
			let txtCount = String(count) + '/' + String(max);
			el.nextElementSibling.querySelector('.count-txt').innerText = txtCount;
		}
	});
};

let setSearchToggle = function setSearchToggle() {
	var icon = document.getElementById('searchIcon');
	var search = document.getElementById('search');
	var input = document.getElementById('algoliaSearch');

	if (!icon) {
		return;
	}

	icon.addEventListener('click', function (event) {
		search.classList.toggle('bd-is-visible');

		if (search.classList.contains('bd-is-visible')) {
			algoliaSearch.focus();
		}
	});

	window.addEventListener('keydown', function (event) {
		if (event.key === 'Escape') {
			return search.classList.remove('bd-is-visible');
		}
	});
};

var resizeTimer = void 0;

// Dropdowns

document.addEventListener('DOMContentLoaded', function () {
	var $dropdowns = getAll('.dropdown:not(.is-hoverable)');
	const elDropItem = getAll('.dropdown-item:not([disabled])');
	const setDropdowns = () => {
		if ($dropdowns.length > 0) {
			$dropdowns.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					let $dropdownHover = getAll('.dropdown:not(.is-hover)');
					$dropdownHover.forEach(function ($el) {
						$el.classList.remove('is-active');
					});
					$el.classList.toggle('is-active');
				});
				$el.addEventListener('mouseenter', function (event) {
					event.stopPropagation();
					$el.classList.toggle('is-hover');
				});
				$el.addEventListener('mouseleave', function (event) {
					event.stopPropagation();
					$el.classList.remove('is-hover');
				});
			});
			document.addEventListener('click', function (event) {
				closeDropdowns();
			});
			elDropItem.forEach($el => {
				$el.addEventListener('click', function (event) {
					event.stopPropagation();
					getSiblings($el).forEach(function (subel, i) {
						subel.classList.remove('is-active');
					});
					$el.classList.add('is-active');
					closest($el, 'dropdown').querySelector('.dropdown-trigger').querySelector('.button').querySelector('span').innerText = $el.innerText;
					closeDropdowns();
				});
			});
		}
	};

	setSearchToggle();
	setDropdowns();

	// scroll to top
	if (document.querySelector('#top')) {
		let toTop = document.querySelector('#top');

		toTop.addEventListener('click', function (e) {
			e.preventDefault();
			window.scroll({top: 0, left: 0, behavior: 'smooth'});
		});
	}

	// Cookies

	// var cookieBookModalName = 'bulma_closed_book_modal';
	//   var cookieBookModal = Cookies.getJSON(cookieBookModalName) || false;

	// Modals
	var rootEl = document.documentElement;
	var $modals = getAll('.modal');
	var $modalButtons = getAll('.modal-button');
	var $modalCloses = getAll('.modal-background, .modal-close, .modal-card-head .delete, .modal-card-foot .button');

	if ($modalButtons.length > 0) {
		$modalButtons.forEach(function ($el) {
			$el.addEventListener('click', function () {
				var target = $el.dataset.target;
				openModal(target);
			});
		});
	}

	if ($modalCloses.length > 0) {
		$modalCloses.forEach(function ($el) {
			$el.addEventListener('click', function () {
				closeModals();
			});
		});
	}

	function openModal(target) {
		var $target = document.getElementById(target);
		rootEl.classList.add('is-clipped');
		$target.classList.add('is-active');
	}

	function closeDropdowns() {
		$dropdowns.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	function closeModals() {
		rootEl.classList.remove('is-clipped');
		$modals.forEach(function ($el) {
			$el.classList.remove('is-active');
		});
	}

	document.addEventListener('keydown', function (event) {
		var e = event || window.event;

		if (e.key === 'Escape') {
			closeModals();
			closeDropdowns();
		}
	});

	// Spinner box
	var setUiSpinner = function uiSpinner() {
		const $spinner = document.querySelectorAll('[data-js-spinner]');

		Array.prototype.forEach.call($spinner, function ($element) {
			const $input = $element.querySelector('[data-js-spinner-input]');
			const $btnDecrement = $element.querySelector('[data-js-spinner-decrement]');
			const $btnIncrement = $element.querySelector('[data-js-spinner-increment]');
			const inputRules = {
				min: +$input.getAttribute('min'),
				max: +$input.getAttribute('max'),
				steps: +$input.getAttribute('steps') || 1,
				maxlength: +$input.getAttribute('maxlength') || null,
				minlength: +$input.getAttribute('minlength') || null,
			};
			let inputValue = +$input.value || 0;

			$input.addEventListener('input', handleInputUpdateValueInput, false);
			$element.addEventListener('keydown', handleMousedownDecrementSpinner, false);
			$btnDecrement.addEventListener('click', handleClickDecrementBtnDecrement, false);
			$btnIncrement.addEventListener('click', handleClickIncrementBtnIncrement, false);

			function handleInputUpdateValueInput() {
				let value = +$input.value;

				if (isNaN(value)) inputValue = 0;
				else inputValue = value;
			}

			function handleMousedownDecrementSpinner(event) {
				const keyCode = event.keyCode;
				const arrowUpKeyCode = 38;
				const arrowDownKeyCode = 40;

				if (keyCode === arrowDownKeyCode) handleClickDecrementBtnDecrement();
				else if (keyCode === arrowUpKeyCode) handleClickIncrementBtnIncrement();
			}

			function handleClickDecrementBtnDecrement() {
				if (!isGreaterThanMaxlength(inputValue - 1)) {
					if ($input.hasAttribute('min')) {
						if (inputValue > inputRules.min) decrement();
					} else decrement();
				}
			}

			function handleClickIncrementBtnIncrement() {
				if (!isGreaterThanMaxlength(inputValue + 1)) {
					if ($input.hasAttribute('max')) {
						if (inputValue < inputRules.max) increment();
					} else increment();
				}
			}

			function decrement() {
				inputValue -= inputRules.steps;
				if ($input.hasAttribute('max') && inputValue > $input.getAttribute('max')) {
					inputValue = +$input.getAttribute('max');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function increment() {
				inputValue += inputRules.steps;
				if ($input.hasAttribute('min') && inputValue < $input.getAttribute('min')) {
					inputValue = +$input.getAttribute('min');
					$input.value = +inputValue.toFixed(12);
				} else $input.value = +inputValue.toFixed(12);
			}

			function isGreaterThanMaxlength(value) {
				return value.toString().length > inputRules.maxlength && inputRules.maxlength !== null;
			}
		});
	};

	setUiSpinner();

	// Collapse / Accordion
	function initCollapse(elem, option) {
		document.addEventListener('click', function (e) {
			if (!e.target.matches(elem + ' [data-collapse]')) return;
			else {
				e.preventDefault();
				let parentEl = e.target.parentElement.parentElement;
				if (parentEl.classList.contains('is-active')) {
					parentEl.classList.remove('is-active');
				} else {
					// accordion
					if (option == true) {
						let accId = parentEl.getAttribute('data-accordion');
						let accEl = document.getElementById(accId);
						let elementList = accEl.querySelectorAll('.collapse-item');
						Array.prototype.forEach.call(elementList, function (e) {
							e.classList.remove('is-active');
						});
					}
					parentEl.classList.add('is-active');
				}
			}
		});
	}
	initCollapse('.collapse.is-accordion', true);
	initCollapse('.collapse.is-collapse', false);

	// search modal popup action
	let elHKeyword = document.querySelectorAll('.keyword-header-search');
	if (elHKeyword) {
		elHKeyword.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				document.querySelector('.header-search-input').value = this.innerText;
			});
		});
	}
	let elHKwDelete = document.querySelectorAll('.button-delete-hkeyword');
	if (elHKwDelete) {
		elHKwDelete.forEach(function (el, key) {
			el.addEventListener('click', function (event) {
				event.stopPropagation();
				closest(this, 'header-search-layer__recent-item').remove();
			});
		});
	}
	// let elHKwDelete = document.querySelectorAll('.button-delete-hkeyword');
	let elHKwDelAll = document.querySelector('.button-delete-hkeyword__all');
	if (elHKwDelAll) {
		elHKwDelAll.addEventListener('click', function (event) {
			event.stopPropagation();
			let elHKwItem = document.querySelectorAll('.header-search-layer__recent-item');
			if (elHKwItem) {
				elHKwItem.forEach(function (el, key) {
					el.remove();
				});
			}
		});
	}

	// password container
	let elPassword = document.querySelectorAll('.password-container');
	if (elPassword) {
		elPassword.forEach(function (el, key) {
			el.querySelector('.password-container-button').addEventListener('click', function (event) {
				event.stopPropagation();
				this.classList.toggle('is-active');
				let passwordInput = el.querySelector('.input');
				if (this.classList.contains('is-active')) {
					passwordInput.type = 'text';
				} else {
					passwordInput.type = 'password';
				}
			});
		});
	}

	//관심등록버튼
	function favoriteButtonToggle(e) {
		if (e.classList.contains('--on')) {
			e.classList.remove('--on');
		} else {
			e.classList.add('--on');
		}
	}

	// Quickmenu + get scroll position
	if (document.querySelector('#header') && document.querySelector('.quickmenu')) {
		let headerH = document.querySelector('#header').clientHeight;
		const funcScrollCheck = () => {
			let scroll = window.scrollY;
			const elQuick = document.querySelector('.quickmenu');
			if (scroll > headerH) {
				elQuick.classList.add('is-sticky');
			} else {
				elQuick.classList.remove('is-sticky');
			}
		};
		funcScrollCheck();

		window.addEventListener('scroll', function () {
			funcScrollCheck();
		});
	}

	// 상품 리스트페이지 favorite toast + is-active 클래스 부여
	if (document.querySelectorAll('.button-list-favorite')) {
		let elVar = document.querySelectorAll('.button-list-favorite');
		elVar.forEach(target => {
			target.addEventListener('click', function (event) {
				let icons = target.querySelectorAll('.mobile-icon')[0];
				event.stopPropagation();
				if (target.classList.contains('is-active')) {
					target.classList.remove('is-active');
					displayToast('Bottom Center', 'is-danger', 'Remove to Favorites.');
					icons.classList.remove('ico-heart-on');
					icons.classList.add('ico-heart-off');
				} else {
					target.classList.add('is-active');
					displayToast('Bottom Center', 'is-success', 'Success! Add to Favorites.');
					icons.classList.remove('ico-heart-off');
					icons.classList.add('ico-heart-on');
				}
			});
		});
	}
});

// 웹접근성 Dynamic Tab
function TabsAutomatic(groupNode) {
	this.tablistNode = groupNode;

	this.tabs = [];

	this.firstTab = null;
	this.lastTab = null;

	// ie11
	let tabs = [];
	new Set(this.tablistNode.querySelectorAll('[role=tab]')).forEach(function (v) {
		tabs.push(v);
	});
	this.tabs = tabs;
	// this.tabs = Array.from(this.tablistNode.querySelectorAll('[role=tab]'));

	this.tabpanels = [];

	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		var tabpanel = document.getElementById(tab.getAttribute('aria-controls'));

		tab.tabIndex = -1;
		tab.setAttribute('aria-selected', 'false');
		this.tabpanels.push(tabpanel);

		tab.addEventListener('keydown', this.onKeydown.bind(this));
		tab.addEventListener('click', this.onClick.bind(this));

		if (!this.firstTab) {
			this.firstTab = tab;
		}
		this.lastTab = tab;
	}
	this.setSelectedTab(this.firstTab, false);
}
TabsAutomatic.prototype.setSelectedTab = function (currentTab, setFocus) {
	if (typeof setFocus !== 'boolean') {
		setFocus = true;
	}
	for (var i = 0; i < this.tabs.length; i += 1) {
		var tab = this.tabs[i];
		if (currentTab === tab) {
			tab.setAttribute('aria-selected', 'true');
			tab.parentElement.classList.add('is-active');
			tab.removeAttribute('tabindex');
			this.tabpanels[i].setAttribute('aria-hidden', 'false');
			this.tabpanels[i].classList.add('is-active');
			if (setFocus) {
				tab.focus();
			}
		} else {
			tab.parentElement.classList.remove('is-active');
			tab.setAttribute('aria-selected', 'false');
			tab.tabIndex = -1;
			this.tabpanels[i].setAttribute('aria-hidden', 'true');
			this.tabpanels[i].classList.remove('is-active');
		}
	}
};
TabsAutomatic.prototype.setSelectedToPreviousTab = function (currentTab) {
	var index;

	if (currentTab === this.firstTab) {
		this.setSelectedTab(this.lastTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index - 1]);
	}
};
TabsAutomatic.prototype.setSelectedToNextTab = function (currentTab) {
	var index;

	if (currentTab === this.lastTab) {
		this.setSelectedTab(this.firstTab);
	} else {
		index = this.tabs.indexOf(currentTab);
		this.setSelectedTab(this.tabs[index + 1]);
	}
};
/* EVENT HANDLERS */
TabsAutomatic.prototype.onKeydown = function (event) {
	var tgt = event.currentTarget,
		flag = false;

	switch (event.key) {
		case 'ArrowLeft':
			this.setSelectedToPreviousTab(tgt);
			flag = true;
			break;

		case 'ArrowRight':
			this.setSelectedToNextTab(tgt);
			flag = true;
			break;

		case 'Home':
			this.setSelectedTab(this.firstTab);
			flag = true;
			break;

		case 'End':
			this.setSelectedTab(this.lastTab);
			flag = true;
			break;

		default:
			break;
	}

	if (flag) {
		event.stopPropagation();
		event.preventDefault();
	}
};
TabsAutomatic.prototype.onClick = function (event) {
	this.setSelectedTab(event.currentTarget);
};
// Initialize tablist
window.addEventListener('load', function () {
	var tablists = document.querySelectorAll('[role=tablist]');
	for (var i = 0; i < tablists.length; i++) {
		// TabsAutomatic(tablists[i]);
		new TabsAutomatic(tablists[i]);
	}
});

// Utils
function getAll(selector) {
	var parent = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : document;

	return Array.prototype.slice.call(parent.querySelectorAll(selector), 0);
}

// var world = document.getElementById('world'); var closest = closest(world, 'item');
// function closest(el, classname) {
// 	if (el.parentNode) {
// 		if (el.parentNode.className.includes(classname)) {
// 			return el.parentNode;
// 		} else {
// 			return closest(el.parentNode, classname);
// 		}
// 	} else {
// 		return false;
// 	}
// }

/*
s : mp menu : Products Categories
*/
// get all siblings
function getSiblings(e) {
	// for collecting siblings
	let siblings = [];
	// if no parent, return no sibling
	if (!e.parentNode) {
		return siblings;
	}
	// first child of the parent node
	let sibling = e.parentNode.firstChild;

	// collecting siblings
	while (sibling) {
		if (sibling.nodeType === 1 && sibling !== e) {
			siblings.push(sibling);
		}
		sibling = sibling.nextSibling;
	}
	return siblings;
}

function extend(a, b) {
	for (var key in b) {
		if (b.hasOwnProperty(key)) {
			a[key] = b[key];
		}
	}
	return a;
}
function hasParent(e, id) {
	if (!e) return false;
	var el = e.target || e.srcElement || e || false;
	while (el && el.id != id) {
		el = el.parentNode || false;
	}
	return el !== false;
}
// returns the depth of the element "e" relative to element with id=id
// for this calculation only parents with classname = waypoint are considered
function getLevelDepth(e, id, waypoint, cnt) {
	cnt = cnt || 0;
	if (e.id.indexOf(id) >= 0) return cnt;

	if (e.classList.contains(waypoint)) {
		++cnt;
	}
	return e.parentNode && getLevelDepth(e.parentNode, id, waypoint, cnt);
}

// http://coveroverflow.com/a/11381730/989439
function mobilecheck() {
	var check = false;
	(function (a) {
		if (
			/(android|ipad|playbook|silk|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
				a,
			) ||
			/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
				a.substr(0, 4),
			)
		)
			check = true;
	})(navigator.userAgent || navigator.vendor || window.opera);
	return check;
}

// returns the closest element to 'e' that has class "classname"
function closest(e, classname) {
	if (e.classList.contains(classname)) {
		return e;
	}
	return e.parentNode && closest(e.parentNode, classname);
}

function mlPushMenu(el, trigger, options) {
	this.el = el;
	this.trigger = trigger;
	this.options = extend(this.defaults, options);
	// support 3d transforms
	this._init();
}

mlPushMenu.prototype = {
	defaults: {
		type: 'overlap', // overlap || cover
		// space between each overlaped level
		levelSpacing: 40,
		backClass: 'mp-back',
	},
	_init: function () {
		this.open = false;
		// this.lastLevel = 1;
		this.level = 0;
		// this.wrapper = document.getElementById('wrap');
		this.wrapper = document.body;
		this.levels = Array.prototype.slice.call(this.el.querySelectorAll('.mp-level'));

		// li 요소별 하위에 mp-level 클래스가 있는 경우 has-child 부여
		this.lis = Array.prototype.slice.call(this.el.querySelectorAll('li'));
		this.lis.forEach(function (el, i) {
			try {
				if (el.querySelector('section').classList.contains('mp-level')) {
					el.classList.add('has-child');
				}
			} catch (error) {
				// console.log(error);
			}
		});

		var self = this;
		this.breakPoint = 1054;
		this.elWidth = 300;
		this.levels.forEach(function (el, i) {
			el.setAttribute('data-level', getLevelDepth(el, self.el.id, 'mp-level'));
		});
		this.menuItems = Array.prototype.slice.call(this.el.querySelectorAll('li'));
		this.levelBack = Array.prototype.slice.call(this.el.querySelectorAll('.' + this.options.backClass));
		this.eventtype = mobilecheck() ? 'click' : 'click';
		this.el.classList.add('mp-' + this.options.type);
		this._initEvents();
	},
	_initEvents: function () {
		var self = this;

		var bodyClickFn = function (el) {
			self._resetMenu();
			el.removeEventListener(self.eventtype, bodyClickFn);
		};

		this.trigger.addEventListener(this.eventtype, function (ev) {
			ev.stopPropagation();
			ev.preventDefault();

			if (self.open) {
				self._resetMenu();
			} else {
				self._openMenu();
				document.addEventListener(self.eventtype, function (ev) {
					if (self.open && !hasParent(ev.target, self.el.id)) {
						bodyClickFn(this);
					}
				});
			}
		});

		this.menuItems.forEach(function (el, i) {
			let subLevel = el.querySelector('.mp-level');

			// a 태그 mouseover
			el.querySelector('a').addEventListener('mouseenter', function (ev) {
				ev.preventDefault();
				let elthis = this;
				let windowWidth = window.outerWidth;
				// let timeoutId = null;
				if (windowWidth > self.breakPoint) {
					ev.stopPropagation();

					// 형제 li 노드 is-hover 제어
					let parentLi = elthis.parentNode;
					parentLi.classList.add('is-hover');
					getSiblings(parentLi).forEach(function (subel, i) {
						subel.classList.remove('is-hover');
					});

					let level = Number(closest(elthis, 'mp-level').getAttribute('data-level'));
					if (level === 1) {
						let thisSubLevel = parentLi.querySelector('.mp-level');
						if (thisSubLevel) {
							if (!thisSubLevel.classList.contains('mp-level-open')) {
								self._openMenu(thisSubLevel);
								thisSubLevel.querySelector('li').classList.add('is-hover');
								let thisLastSubLevel = thisSubLevel.querySelector('li').querySelector('.mp-level');
								self._openMenu(thisLastSubLevel);
							}
						}
					}
					// clearTimeout(timeoutId);
					// timeoutId = setTimeout(() => {
					// 	let level = Number(closest(elthis, 'mp-level').getAttribute('data-level'));
					// 	if (self.level <= level) {
					// 		self.level = level;
					// 		self.level === 0 ? self._resetMenu() : self._closeMenu();
					// 	}
					// 	try {
					// 		if (this.nextElementSibling.classList.contains('mp-level-open')) {
					// 			level += 1;
					// 		}
					// 	} catch (error) {
					// 		// console.log(error);
					// 	}
					// 	self.lastLevel = level;
					// 	self.el.setAttribute('data-last-level', self.lastLevel);
					// }, 50);

					getSiblings(this.parentNode).forEach(function (subel, i) {
						// is-hover 클래스 전체 삭제
						self._resetHover(subel.querySelectorAll('li'));
						if (subel.querySelector('.mp-level')) {
							subel.querySelector('.mp-level').classList.remove('mp-level-open');
						}
					});

					self._openMenu(subLevel);
				}
			});
			// el.querySelector('a').addEventListener('mouseenter', function (ev) {
			// 	ev.preventDefault();
			// 	let windowWidth = window.outerWidth;
			// 	if (windowWidth > self.breakPoint) {
			// 		// li 요소의 형제 요소들 모두 mp-level-open 제거 후 본인에만 추가
			// 		el.classList.add('is-hover');
			// 		getSiblings(el).forEach(function (subel, i) {
			// 			subel.classList.remove('is-hover');
			// 		});
			// 	}
			// });
			if (subLevel) {
				// 1depth 메뉴 a 태그 클릭시
				el.querySelector('a').addEventListener(self.eventtype, function (ev) {
					if (ev.cancelable) ev.preventDefault();
					var level = closest(el, 'mp-level').getAttribute('data-level');
					if (self.level <= level) {
						ev.stopPropagation();
						closest(el, 'mp-level').classList.add('mp-level-overlay');
						self._openMenu(subLevel);
					}
				});
			}
		});

		this.levels.forEach(function (el, i) {
			el.addEventListener(self.eventtype, function (ev) {
				ev.stopPropagation();
				let level = el.getAttribute('data-level');
				if (self.level > level) {
					self.level = level;
					self._closeMenu();
				}
			});
			// el.addEventListener('mouseenter', function (ev) {
			// 	ev.stopPropagation();
			// 	let windowWidth = window.outerWidth;
			// 	if (windowWidth > self.breakPoint) {
			// 		ev.preventDefault();
			// 		let level = closest(el, 'mp-level').getAttribute('data-level');
			// 		if (self.level <= level) {
			// 			ev.stopPropagation();
			// 			self.level = closest(el, 'mp-level').getAttribute('data-level');
			// 			self.level === 0 ? self._resetMenu() : self._closeMenu();
			// 		}
			// 	}
			// });
		});

		this.levelBack.forEach(function (el, i) {
			el.addEventListener(self.eventtype, function (ev) {
				ev.preventDefault();
				var level = closest(el, 'mp-level').getAttribute('data-level');
				if (self.level <= level) {
					ev.stopPropagation();
					self.level = closest(el, 'mp-level').getAttribute('data-level') - 1;
					self.level === 0 ? self._resetMenu() : self._closeMenu();
				}
			});
		});

		// 카테고리 메뉴 닫기 버튼
		if (document.querySelector('.button-close-allmenu')) {
			document.querySelector('.button-close-allmenu').addEventListener('click', function (e) {
				e.preventDefault();
				e.stopPropagation();
				self._resetMenu();
				self._closeMenu();
			});
		}
	},
	_openMenu: function (subLevel) {
		++this.level;

		var levelFactor = (this.level - 1) * this.options.levelSpacing,
			translateVal = this.options.type === 'overlap' ? this.elWidth + levelFactor : this.elWidth;
		this._setTransform('translate3d(' + translateVal + 'px,0,0)');

		if (subLevel) {
			this._setTransform('', subLevel);
			for (var i = 0, len = this.levels.length; i < len; ++i) {
				var levelEl = this.levels[i];
				// console.log(' levelEl ', levelEl);
				if (levelEl != subLevel && !levelEl.classList.contains('mp-level-open')) {
					this._setTransform('translate3d(-100%,0,0) translate3d(' + -1 * levelFactor + 'px,0,0)', levelEl);
				}
			}
		}
		if (this.level === 1) {
			document.querySelector('html').classList.add('is-clipped');
			this.wrapper.classList.add('__allmenu-open');
			// 첫번째 li 요소에 is-hover + mp-level-open
			let firstLi = document.querySelector('.allmenu-list').querySelector('li');
			firstLi.classList.add('is-hover');
			// mobile/tablet 이 아닌 경우에만 첫번째 메뉴 오픈
			if (!mobilecheck()) {
				firstLi.querySelector('.mp-level').classList.add('mp-level-open');
			}
			this.open = true;
		}
		(subLevel || this.levels[0]).classList.add('mp-level-open');
	},
	_resetMenu: function () {
		this._setTransform('translate3d(0,0,0)');
		this.level = 0;
		// remove class mp-pushed from main wrapper
		document.querySelector('html').classList.remove('is-clipped');
		this.wrapper.classList.remove('__allmenu-open');
		this._toggleLevels();
		this.open = false;
		//document.querySelectorAll('selector').forEach(sss => { {2:box}.style.display = "none" });
		// is-hover 클래스 전체 삭제
		try {
			this._resetHover(this.el.querySelectorAll('li'));
		} catch (error) {
			console.log(error);
		}
	},
	_resetHover: function (el) {
		// is-hover 클래스 전체 삭제
		let els = Array.prototype.slice.call(el);
		els.forEach(function (el, i) {
			try {
				el.classList.remove('is-hover');
			} catch (error) {
				console.log(error);
			}
		});
	},
	_closeMenu: function () {
		var translateVal = this.options.type === 'overlap' ? this.elWidth + (this.level - 1) * this.options.levelSpacing : this.elWidth;
		this._setTransform('translate3d(' + translateVal + 'px,0,0)');
		this._toggleLevels();
	},
	_setTransform: function (val, el) {
		// el = el || this.wrapper;
		el = el || this.el;
		el.style.WebkitTransform = val;
		el.style.MozTransform = val;
		el.style.transform = val;
	},
	_toggleLevels: function () {
		for (var i = 0, len = this.levels.length; i < len; ++i) {
			var levelEl = this.levels[i];
			if (levelEl.getAttribute('data-level') >= this.level + 1) {
				levelEl.classList.remove('mp-level-open');
				levelEl.classList.remove('mp-level-overlay');
			} else if (Number(levelEl.getAttribute('data-level')) == this.level) {
				levelEl.classList.remove('mp-level-overlay');
			}
		}
	},
};

/*
	Window resize handler
	*/
// window resize
function windowResize() {
	window.clearTimeout(resizeTimer);

	// 1038px -> 1054px
	// let windowWidth = window.outerWidth;
	// if (windowWidth > 1054) {
	// 	console.log(windowWidth);
	// }

	// resizeTimer = window.setTimeout(function () {
	// 	setNavbarVisibility();
	// }, 10);
}

// add to global namespace
document.addEventListener('DOMContentLoaded', function () {
	if (document.getElementById('mp-menu')) {
		window.mlPushMenu = mlPushMenu;
		new mlPushMenu(document.getElementById('mp-menu'), document.getElementById('trigger'), {
			type: 'cover',
		});
	}

	// Events
	// windowResize();
	// window.addEventListener('resize', windowResize);
});
/*
 e : mp menu
*/

document.addEventListener('DOMContentLoaded', function () {
	// 기업상세 즐겨찾기
	// heart click event
	if (document.querySelectorAll('.favorite-btn')) {
		let heartBtn = document.querySelectorAll('.favorite-btn');
		heartBtn.forEach(target => {
			target.addEventListener('click', () => {
				target.classList.toggle('is-active');
			});
		});
	}

	// 검색 값 초기화
	if (document.querySelector('.fave-clear-btn')) { 
		let clearBtn = document.querySelector('.fave-clear-btn');
		let searchText = document.querySelectorAll('.fave-search-input');

		clearBtn.addEventListener('click', () => {
			searchText.forEach(target => {
				target.value = '';
			});
		});
	}

	// 체크박스 전체 선택 및 해제
	if (document.querySelector('.all-checkbox')) {
		let selCheck = document.querySelectorAll('.all-checkbox-item');
		let allCheck = document.querySelector('.all-checkbox');

		// all check box check event
		allCheck.addEventListener('click', event => {
			if (allCheck.checked) {
				selCheck.forEach(target => {
					target.checked = true;
				});
			} else {
				selCheck.forEach(target => {
					target.checked = false;
				});
			}
		});
	}

	// share click event
	if (document.querySelectorAll('.share-btn')) {
		let shareBtn = document.querySelectorAll('.share-btn');
		shareBtn.forEach(target => {
			target.addEventListener('click', () => {
				target.classList.toggle('is-active');
			});
		});
	}

	// join textarea count
	if (document.getElementById('joinIntroduction')) {
		setTextCount('joinIntroduction', 2000);
	}

	// product detail textarea count
	if (document.getElementById('inqTextarea')) {
		setTextCount('inqTextarea', 10000);
	}

	if (document.querySelectorAll('.prod-tag-btn')) {
		document.querySelectorAll('.prod-tag-btn').forEach(target => {
			target.addEventListener('click', () => {
				document.querySelectorAll('.prod-tag-btn').forEach(event => {
					event.classList.remove('on-active');
				});
				target.classList.add('on-active');
			});
		});
	}

	if (document.querySelector('.basic-tabs-ul')) {
		document.querySelectorAll('.basic-tabs-ul li').forEach(target => {
			target.addEventListener('click', () => {
				document.querySelectorAll('.basic-tabs-ul li').forEach(event => {
					event.classList.remove('is-active');
				});
				target.classList.add('is-active');
			});
		});
	}

	if (document.querySelectorAll('.ctg-ver-item')) {
		let ctgVerItem = document.querySelectorAll('.ctg-ver-item');

		ctgVerItem.forEach(function (item) {
			item.addEventListener('click', function (e) {
				ctgVerItem.forEach(function (ctg) {
					ctg.classList.remove('is-active');
				});
				item.classList.add('is-active');
			});
		});
	}

	if (document.querySelectorAll('.category-mobile-button')) {
		let mobileCtgBtn = document.querySelectorAll('.category-mobile-button');
		let mobileTagBox = document.querySelector('.category-mobile');

		mobileCtgBtn.forEach(function (btn) {
			btn.addEventListener('click', function (e) {
				if (btn.parentElement.classList.contains('is-open')) {
					// btn.classList.add('is-active');
					btn.parentElement.classList.remove('is-open');
					// btn.parentElement.style.maxHeight = 600 + 'px';
				} else {
					// btn.classList.remove('is-active');
					btn.parentElement.classList.add('is-open');
					// btn.parentElement.style.maxHeight = tagBox + 'px';
				}
			});
		});
	}

	// 카탈로그 선택/다운로드 작동
	if (document.querySelector('.button-select-catalog') && document.querySelector('.list__prod')) {
		let elSelCatl = document.querySelector('.button-select-catalog');
		const checkAll = mode => {
			const elChkAll = document.querySelector('.list__prod').querySelectorAll('.checkbox-area');
			elChkAll.forEach(target => {
				if (mode === 'add') {
					target.classList.add('is-active');
				} else {
					target.classList.remove('is-active');
				}
			});
		};

		elSelCatl.addEventListener('click', function (e) {
			e.stopPropagation();
			if (elSelCatl.classList.contains('is-active')) {
				elSelCatl.classList.remove('is-active');
				elSelCatl.innerText = 'Select Catalog';
				checkAll('remove');
			} else {
				elSelCatl.classList.add('is-active');
				elSelCatl.innerText = 'Download Catalog';
				checkAll('add');
			}
		});
	}

	// 카테고리 필터 리셋 버튼 동장
	if (document.querySelectorAll('.filter-reset')) {
		const elFilterRst = document.querySelectorAll('.filter-reset');
		elFilterRst.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				target.classList.add('is-loading');
				setTimeout(() => {
					target.classList.remove('is-loading');
				}, 1000);
			});
		});
	}

	// 카테고리 필터 open/close
	if (document.querySelectorAll('.filter-mobile-button')) {
		const elFilterMBtn = document.querySelectorAll('.filter-mobile-button');
		const elFilterClose = document.querySelectorAll('.filter-close');
		const elFilterCancel = document.querySelectorAll('.filter-cancel');
		const elListNav = document.querySelector('.comp-list-nav');
		const elHtml = document.querySelector('html');

		elFilterMBtn.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				elListNav.classList.toggle('is-active');
				elHtml.classList.toggle('is-clipped');
			});
		});
		elFilterClose.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				elListNav.classList.toggle('is-active');
				elHtml.classList.toggle('is-clipped');
			});
		});
		elFilterCancel.forEach(target => {
			target.addEventListener('click', function (event) {
				event.stopPropagation();
				elListNav.classList.toggle('is-active');
				elHtml.classList.toggle('is-clipped');
			});
		});
	}

	// 인콰이어리 슬라이드 inquire slide
	if (document.querySelector('.inquire-wrap.is-slide')) {
		let inqSlideBtn = document.querySelector('.button-inquire-more');
		let inqSlideBtnClose = document.querySelector('.button-inquire-cancel');
		let inqSlide = document.querySelector('.inquire-wrap.is-slide');
		inqSlideBtn.addEventListener('click', function () {
			inqSlide.classList.add('is-active');
		});
		inqSlideBtnClose.addEventListener('click', function () {
			inqSlide.classList.remove('is-active');
		});
	}

	// 상품상세 anchor tabs scroll
	if (document.querySelector('.tabs.is-anchor')) {
		document.querySelectorAll('a[role=anchorlink]').forEach(elem => {
			elem.addEventListener('click', e => {
				e.preventDefault();
				let activeTab = e.target;
				for (; activeTab.nodeName != 'LI'; activeTab = activeTab.parentElement);

				let allTab = activeTab.parentElement.querySelectorAll('li');
				allTab.forEach(event => {
					event.classList.remove('is-active');
				});
				activeTab.classList.add('is-active');

				let block = document.querySelector(elem.getAttribute('href')),
					offset = elem.dataset.offset ? parseInt(elem.dataset.offset) : 0,
					bodyOffset = document.body.getBoundingClientRect().top;
				window.scrollTo({
					top: block.getBoundingClientRect().top - bodyOffset + offset - 100,
					behavior: 'smooth',
				});
			});
		});
	}
});

// Toast message
function displayToast(position, type, msg) {
	bulmaToast.toast({
		message: msg,
		type: type,
		position: position.toLowerCase().replace(' ', '-'),
		dismissible: true,
		duration: 2000,
		pauseOnHover: true,
		animate: {in: 'fadeIn', out: 'fadeOut'},
	});
}
